package com.mlaguna.twitch.gui;

import com.mlaguna.twitch.*;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Controlador implements ActionListener, ListSelectionListener, FocusListener, ItemListener {
    private Vista vista;
    private Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        addActionListener(this);
        addListSelectionListener(this);
        addItemListeners(this);
    }

    private void addListSelectionListener(Controlador controlador) {
        vista.listUsuarios.addListSelectionListener(controlador);
        vista.listStreamers.addListSelectionListener(controlador);
        vista.listSuscripciones.addListSelectionListener(controlador);
        vista.listJuegos.addListSelectionListener(controlador);
        vista.listClips.addListSelectionListener(controlador);
        vista.listConfiguraciones.addListSelectionListener(controlador);
    }

    private void addActionListener(ActionListener listener) {
        vista.conexionItem.addActionListener(listener);
        vista.salirItem.addActionListener(listener);
        vista.btnAltaUsuarios.addActionListener(listener);
        vista.btnAltaStreamer.addActionListener(listener);
        vista.btnAltaSuscripcion.addActionListener(listener);
        vista.btnAltaJuego.addActionListener(listener);
        vista.btnAltaClip.addActionListener(listener);
        vista.btnAltaConfiguracion.addActionListener(listener);
        vista.btnModificarUsuario.addActionListener(listener);
        vista.btnModificarStreamer.addActionListener(listener);
        vista.btnModificarSuscripcion.addActionListener(listener);
        vista.btnModificarJuego.addActionListener(listener);
        vista.btnModificarClip.addActionListener(listener);
        vista.btnModificarConfiguracion.addActionListener(listener);
        vista.btnEliminarUsuario.addActionListener(listener);
        vista.btnEliminarStreamer.addActionListener(listener);
        vista.btnEliminarSuscripcion.addActionListener(listener);
        vista.btnEliminarJuego.addActionListener(listener);
        vista.btnEliminarClip.addActionListener(listener);
        vista.btnEliminarConfiguracion.addActionListener(listener);
        vista.rbPrimeUsuarioSi.addActionListener(listener);
        vista.rbPrimeUsuarioNo.addActionListener(listener);
        vista.rbPrivadoClipSi.addActionListener(listener);
        vista.rbiPrivadoClipNo.addActionListener(listener);
        vista.cbOcultoConfiguracion.addActionListener(listener);
        vista.cbTemaConfiguracion.addActionListener(listener);
        vista.btnAddStreamerAUsuario.addActionListener(listener);
    }

    private void addItemListeners(ItemListener listener){
        vista.cbStreamersEnUsuarios.addItemListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch (comando) {
            case "Salir":
                modelo.desconectar();
                System.exit(0);
                break;
            case "Conectar":
                modelo.conectar();
                listarUsuarios(modelo.getUsuarios());
                listarStreamers(modelo.getStreamers());
                listarSuscripcion(modelo.getSuscripciones());
                listarJuego(modelo.getJuegos());
                listarClip(modelo.getClips());
                listarConfiguracion(modelo.getConfiguraciones());
                listarStreamersEnComboBox();
                break;
            case "Alta Usuario":
                nuevoUsuario();
                listarUsuarios(modelo.getUsuarios());
                limpiarCamposUsuario();
                break;
            case "Modificar Usuario":
                modificarUsuario();
                listarUsuarios(modelo.getUsuarios());
                limpiarCamposUsuario();
                break;
            case "Eliminar Usuario":
                eliminarUsuario();
                listarUsuarios(modelo.getUsuarios());
                limpiarCamposUsuario();
                break;
            case "Alta Streamer":
                nuevoStreamer();
                listarStreamers(modelo.getStreamers());
                limpiarCamposStreamer();
                listarStreamersEnComboBox();
                break;
            case "Modificar Streamer":
                modificarStreamer();
                listarStreamers(modelo.getStreamers());
                limpiarCamposStreamer();
                listarStreamersEnComboBox();
                break;
            case "Eliminar Streamer":
                eliminarStreamer();
                listarStreamers(modelo.getStreamers());
                limpiarCamposStreamer();
                listarStreamersEnComboBox();
                break;
            case "Alta Suscripcion":
                nuevaSuscripcion();
                listarSuscripcion(modelo.getSuscripciones());
                limpiarCamposSuscripciones();
                break;
            case "Modificar Suscripcion":
                modificarSuscripcion();
                listarSuscripcion(modelo.getSuscripciones());
                limpiarCamposSuscripciones();
                break;
            case "Eliminar Suscripcion":
                eliminarSuscripcion();
                listarSuscripcion(modelo.getSuscripciones());
                limpiarCamposSuscripciones();
                break;
            case "Alta Juego":
                nuevoJuego();
                listarJuego(modelo.getJuegos());
                limpiarCamposJuego();
                break;
            case "Modificar Juego":
                modificarJuego();
                listarJuego(modelo.getJuegos());
                limpiarCamposJuego();
                break;
            case "Eliminar Juego":
                eliminarJuego();
                listarJuego(modelo.getJuegos());
                limpiarCamposJuego();
                break;
            case "Alta Clip":
                nuevoClip();
                listarClip(modelo.getClips());
                limpiarCamposClip();
                break;
            case "Modificar Clip":
                modificarClip();
                listarClip(modelo.getClips());
                limpiarCamposClip();
                break;
            case "Eliminar Clip":
                eliminarClip();
                listarClip(modelo.getClips());
                limpiarCamposClip();
                break;
            case "Alta Configuracion":
                nuevaConfiguracion();
                listarConfiguracion(modelo.getConfiguraciones());
                limpiarCamposConfiguracion();
                break;
            case "Modificar Configuracion":
                modificarConfiguracion();
                listarConfiguracion(modelo.getConfiguraciones());
                limpiarCamposConfiguracion();
                break;
            case "Eliminar Configuracion":
                eliminarConfiguracion();
                listarConfiguracion(modelo.getConfiguraciones());
                limpiarCamposConfiguracion();
                break;
        }
    }

    private void listarStreamersEnComboBox() {
        List<Streamer> listaStreamers = modelo.getStreamers();
        vista.dcbmStreamers.removeAllElements();
        for (Streamer streamer: listaStreamers) {
            vista.dcbmStreamers.addElement(streamer);
        }
    }


    private void limpiarCamposUsuario() {
        vista.txtNombreUsuario.setText("");
        vista.rbPrimeUsuarioSi.setSelected(false);
        vista.rbPrimeUsuarioNo.setSelected(false);
    }

    private void limpiarCamposStreamer() {
        vista.txtNombreStreamer.setText("");
        vista.txtSeguidoresStreamer.setText("");
    }

    private void limpiarCamposSuscripciones() {
        vista.txtCantidadSuscripcion.setText("");
        vista.dpFechaLimiteSuscripcion.setDateTimePermissive(null);
    }

    private void limpiarCamposJuego() {
        vista.txtCategoriaJuego.setText("");
        vista.txtTituloJuego.setText("");
    }

    private void limpiarCamposClip() {
        vista.txtTituloClip.setText("");
        vista.txtDuracionClip.setText("");
        vista.rbPrivadoClipSi.setSelected(false);
        vista.rbiPrivadoClipNo.setSelected(false);
    }

    private void limpiarCamposConfiguracion() {
        vista.cbTemaConfiguracion.setSelected(false);
        vista.txtFotoConfiguracion.setText("");
        vista.cbOcultoConfiguracion.setSelected(false);
    }

    private void eliminarConfiguracion() {
        Configuracion configuracionBorrada = (Configuracion) vista.listConfiguraciones.getSelectedValue();
        modelo.borrarConfiguracion(configuracionBorrada);
    }

    private void eliminarClip() {
        Clip clipBorrado = (Clip) vista.listClips.getSelectedValue();
        modelo.borrarClip(clipBorrado);
    }

    private void eliminarJuego() {
        Juego juegoBorrado = (Juego) vista.listJuegos.getSelectedValue();
        modelo.borrarJuego(juegoBorrado);
    }

    private void eliminarSuscripcion() {
        Suscripcion suscripcionBorrado = (Suscripcion) vista.listSuscripciones.getSelectedValue();
        modelo.borrarSuscripcion(suscripcionBorrado);
    }

    private void eliminarStreamer() {
        Streamer streamerBorrado = (Streamer) vista.listStreamers.getSelectedValue();
        modelo.borrarStreamer(streamerBorrado);
    }

    private void eliminarUsuario() {
        Usuario usuarioBorrado = (Usuario) vista.listUsuarios.getSelectedValue();
        modelo.borrarUsuario(usuarioBorrado);
    }

    private void modificarConfiguracion() {
        Configuracion configuracionSeleccion = (Configuracion) vista.listConfiguraciones.getSelectedValue();
        if (vista.cbTemaConfiguracion.isSelected()) {
            configuracionSeleccion.setTema(true);
        } else {
            configuracionSeleccion.setTema(false);
        }
        if (vista.cbOcultoConfiguracion.isSelected()) {
            configuracionSeleccion.setOculto(true);
        } else {
            configuracionSeleccion.setOculto(false);
        }
        configuracionSeleccion.setFoto(vista.txtFotoConfiguracion.getText());
        modelo.modificarConfiguracion(configuracionSeleccion);
    }

    private void modificarClip() {
        Clip clipSeleccion = (Clip) vista.listClips.getSelectedValue();
        clipSeleccion.setTitulo(vista.txtTituloClip.getText());
        clipSeleccion.setDuracion(Integer.parseInt(vista.txtDuracionClip.getText()));
        if (vista.rbPrivadoClipSi.isSelected()) {
            clipSeleccion.setPrivado(true);
        } else {
            clipSeleccion.setPrivado(false);
        }
        modelo.modificarClip(clipSeleccion);
    }

    private void modificarJuego() {
        Juego juegoSeleccion = (Juego) vista.listJuegos.getSelectedValue();
        juegoSeleccion.setCategoria(vista.txtCategoriaJuego.getText());
        juegoSeleccion.setTitulo(vista.txtTituloJuego.getText());
        modelo.modificarJuego(juegoSeleccion);
    }

    private void modificarSuscripcion() {
        Suscripcion suscripcionSeleccion = (Suscripcion) vista.listSuscripciones.getSelectedValue();
        suscripcionSeleccion.setCantidad(Integer.parseInt(vista.txtCantidadSuscripcion.getText()));
        suscripcionSeleccion.setFechaLimite(vista.dpFechaLimiteSuscripcion.getDateTimePermissive());
        modelo.modificarSuscripcion(suscripcionSeleccion);
    }

    private void modificarStreamer() {
        Streamer streamerSeleccion = (Streamer) vista.listStreamers.getSelectedValue();
        streamerSeleccion.setNombre(vista.txtNombreStreamer.getText());
        streamerSeleccion.setSeguidores(Integer.parseInt(vista.txtSeguidoresStreamer.getText()));
        modelo.modificarStreamer(streamerSeleccion);
    }

    private void modificarUsuario() {
        Usuario usuarioSeleccion = (Usuario) vista.listUsuarios.getSelectedValue();
        usuarioSeleccion.setNombre(vista.txtNombreUsuario.getText());
        if (vista.rbPrimeUsuarioSi.isSelected()) {
            usuarioSeleccion.setPrime(true);
        } else {
            usuarioSeleccion.setPrime(false);
        }
        modelo.modificarUsuario(usuarioSeleccion);
    }

    private void nuevaConfiguracion() {
        Configuracion nuevaConfiguracion = new Configuracion();
        setDatosConfiguracion(nuevaConfiguracion);
        modelo.guardarConfiguracion(nuevaConfiguracion);
        vista.dtmConfiguraciones.addElement(nuevaConfiguracion);
        vista.listConfiguraciones.setSelectedValue(nuevaConfiguracion, true);
    }

    private void setDatosConfiguracion(Configuracion configuracion) {
        if (vista.cbTemaConfiguracion.isSelected()) {
            configuracion.setTema(true);
        } else {
            configuracion.setTema(false);
        }
        configuracion.setFoto(vista.txtFotoConfiguracion.getText());
        if (vista.cbOcultoConfiguracion.isSelected()) {
            configuracion.setOculto(true);
        } else {
            configuracion.setOculto(false);
        }
    }

    private void nuevoClip() {
        Clip nuevoClip = new Clip();
        setDatosClip(nuevoClip);
        modelo.guardarClip(nuevoClip);
        vista.dtmClips.addElement(nuevoClip);
        vista.listClips.setSelectedValue(nuevoClip, true);
    }

    private void setDatosClip(Clip clip) {
        clip.setTitulo(vista.txtTituloClip.getText());
        clip.setDuracion(Integer.parseInt(vista.txtDuracionClip.getText()));
        if (vista.rbPrivadoClipSi.isSelected()) {
            clip.setPrivado(true);
        } else {
            clip.setPrivado(false);
        }
    }

    private void nuevoJuego() {
        Juego nuevoJuego = new Juego();
        setDatosJuego(nuevoJuego);
        modelo.guardarJuegos(nuevoJuego);
        vista.dtmJuegos.addElement(nuevoJuego);
        vista.listJuegos.setSelectedValue(nuevoJuego, true);
    }

    private void setDatosJuego(Juego juego) {
        juego.setCategoria(vista.txtCategoriaJuego.getText());
        juego.setTitulo(vista.txtTituloJuego.getText());
    }

    private void nuevaSuscripcion() {
        Suscripcion nuevaSuscripcion = new Suscripcion();
        setDatosSuscripcion(nuevaSuscripcion);
        modelo.guardarSuscripcion(nuevaSuscripcion);
        vista.dtmSuscripciones.addElement(nuevaSuscripcion);
        vista.listSuscripciones.setSelectedValue(nuevaSuscripcion, true);
    }

    private void setDatosSuscripcion(Suscripcion suscripcion) {
        suscripcion.setCantidad(Integer.parseInt(vista.txtCantidadSuscripcion.getText()));
        suscripcion.setFechaLimite(vista.dpFechaLimiteSuscripcion.getDateTimePermissive());
    }

    private void nuevoStreamer() {
        Streamer nuevoStreamer = new Streamer();
        setDatosStreamer(nuevoStreamer);
        modelo.guardarStreamer(nuevoStreamer);
        vista.listStreamers.setSelectedValue(nuevoStreamer, true);
    }

    private void setDatosStreamer(Streamer streamer) {
        streamer.setNombre(vista.txtNombreStreamer.getText());
        streamer.setSeguidores(Integer.parseInt(vista.txtSeguidoresStreamer.getText()));
    }

    private void nuevoUsuario() {
        Usuario nuevoUsuario = new Usuario();
        setDatosUsuario(nuevoUsuario);
        modelo.guardarUsuario(nuevoUsuario);
        vista.listUsuarios.setSelectedValue(nuevoUsuario, true);
    }

    private void setDatosUsuario(Usuario usuario) {
        usuario.setNombre(vista.txtNombreUsuario.getText());
        if (vista.rbPrimeUsuarioSi.isSelected()) {
            usuario.setPrime(true);
        } else {
            usuario.setPrime(false);
        }
    }

    public void listarUsuarios(List<Usuario> lista) {
        vista.dtmUsuarios.clear();
        for (Usuario unUsuario :
                lista) {
            vista.dtmUsuarios.addElement(unUsuario);
        }
    }

    public void listarStreamers(List<Streamer> lista) {
        vista.dtmStreamers.clear();
        for (Streamer unStreamer :
                lista) {
            vista.dtmStreamers.addElement(unStreamer);
        }
    }

    public void listarSuscripcion(List<Suscripcion> lista) {
        vista.dtmSuscripciones.clear();
        for (Suscripcion unaSuscripcion :
                lista) {
            vista.dtmSuscripciones.addElement(unaSuscripcion);
        }
    }

    public void listarJuego(List<Juego> lista) {
        vista.dtmJuegos.clear();
        for (Juego unJuego :
                lista) {
            vista.dtmJuegos.addElement(unJuego);
        }
    }

    public void listarClip(List<Clip> lista) {
        vista.dtmClips.clear();
        for (Clip unClip :
                lista) {
            vista.dtmClips.addElement(unClip);
        }
    }

    public void listarConfiguracion(List<Configuracion> lista) {
        vista.dtmConfiguraciones.clear();
        for (Configuracion unaConfiguracion :
                lista) {
            vista.dtmConfiguraciones.addElement(unaConfiguracion);
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
       if (e.getSource() == vista.listUsuarios) {
            if (!vista.listUsuarios.isSelectionEmpty()) {
                Usuario unUsuario = (Usuario) vista.listUsuarios.getSelectedValue();
                mostrarCamposUsuario(unUsuario);
            }
        }
        else if (e.getSource() == vista.listStreamers){
            if (!vista.listStreamers.isSelectionEmpty()){
                Streamer unStreamer = (Streamer) vista.listStreamers.getSelectedValue();
                mostrarCamposStreamer(unStreamer);
            }
        }
        else if (e.getSource() == vista.listSuscripciones){
            if (!vista.listSuscripciones.isSelectionEmpty()){
                Suscripcion unaSuscripcion = (Suscripcion) vista.listSuscripciones.getSelectedValue();
                mostrarCamposSuscripciones(unaSuscripcion);
            }
       }
        else if (e.getSource() == vista.listJuegos){
            if (!vista.listJuegos.isSelectionEmpty()){
                Juego unJuego = (Juego) vista.listJuegos.getSelectedValue();
                mostrarCamposJuego(unJuego);
            }
       }
        else if (e.getSource() == vista.listClips){
            if (!vista.listClips.isSelectionEmpty()){
                Clip unClip = (Clip) vista.listClips.getSelectedValue();
                mostrarCamposClip(unClip);
            }
       }
        else if (e.getSource() == vista.listConfiguraciones){
            if (!vista.listConfiguraciones.isSelectionEmpty()){
                Configuracion unaConfiguracion = (Configuracion) vista.listConfiguraciones.getSelectedValue();
                mostrarCamposConfiguracion(unaConfiguracion);
            }
       }
    }

    /*@Override
    public void stateChanged(ChangeEvent e) {
        int panelSeleccionado = vista.tbPrincipal.getSelectedIndex();
        switch(panelSeleccionado){
            case 0:
                limpiarCamposUsuario();
                listarUsuarios();
        }
    }*/

    private void mostrarCamposUsuario(Usuario unUsuario) {
        vista.txtNombreUsuario.setText(unUsuario.getNombre());
        if (unUsuario.isPrime()){
            vista.rbPrimeUsuarioSi.setSelected(true);
        }
        else{
            vista.rbPrimeUsuarioNo.setSelected(true);
        }
    }

    private void mostrarCamposStreamer(Streamer unStreamer) {
       vista.txtNombreStreamer.setText(unStreamer.getNombre());
       vista.txtSeguidoresStreamer.setText(String.valueOf(unStreamer.getSeguidores()));
    }

    private void mostrarCamposSuscripciones(Suscripcion unaSuscripcion) {
        vista.txtCantidadSuscripcion.setText(String.valueOf(unaSuscripcion.getCantidad()));
        vista.dpFechaLimiteSuscripcion.setDateTimePermissive(unaSuscripcion.getFechaLimite());
    }

    private void mostrarCamposJuego(Juego unJuego) {
        vista.txtCategoriaJuego.setText(unJuego.getCategoria());
        vista.txtTituloJuego.setText(unJuego.getTitulo());
    }

    private void mostrarCamposClip(Clip unCLip) {
        vista.txtTituloClip.setText(unCLip.getTitulo());
        vista.txtDuracionClip.setText(String.valueOf(unCLip.getDuracion()));
        if (unCLip.isPrivado()){
            vista.rbPrivadoClipSi.setSelected(true);
        }
        else{
            vista.rbiPrivadoClipNo.setSelected(true);
        }
    }

    private void mostrarCamposConfiguracion(Configuracion unaConfiguracion) {
        vista.txtFotoConfiguracion.setText(unaConfiguracion.getFoto());
        if (unaConfiguracion.isOculto()){
            vista.cbOcultoConfiguracion.setSelected(true);
        }
        else{
            vista.cbOcultoConfiguracion.setSelected(false);
        }
        if (unaConfiguracion.isTema()){
            vista.cbTemaConfiguracion.setSelected(true);
        }
        else{
            vista.cbTemaConfiguracion.setSelected(false);
        }
    }

    @Override
    public void focusGained(FocusEvent e) {
        if (e.getSource() == vista.cbStreamersEnUsuarios){
            vista.cbStreamersEnUsuarios.hidePopup();
            listarStreamersEnComboBox();
            vista.cbStreamersEnUsuarios.showPopup();
        }
    }

    @Override
    public void focusLost(FocusEvent e) {

    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }
}


