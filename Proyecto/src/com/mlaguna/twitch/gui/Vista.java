package com.mlaguna.twitch.gui;

import com.github.lgooddatepicker.components.DateTimePicker;
import com.mlaguna.twitch.*;

import javax.swing.*;

public class Vista extends JFrame {
    //private final JFrame frame;
    JTabbedPane tbPrincipal;
    private JPanel panel1;
    JPanel tbStreamers;
    JPanel tbUsuarios;
    JPanel tbSuscripciones;
    JPanel tbJuegos;
    JPanel tbClips;
    JPanel tbConfiguraciones;
    JTextField txtNombreUsuario;
    JButton btnAltaUsuarios;
    JButton btnModificarUsuario;
    JButton btnEliminarUsuario;
    JList listStreamersUsuarios;
    JButton btnListarStreamersUsuario;
    JList listSuscripcionesUsuarios;
    JButton btnListarSuscripcionesUsuarios;
    JTextField txtNombreStreamer;
    JTextField txtSeguidoresStreamer;
    JList listUsuariosStreamers;
    JButton btnListarUsuariosStreamers;
    JButton btnAltaStreamer;
    JButton btnModificarStreamer;
    JButton btnEliminarStreamer;
    JList listClipsStreamers;
    JButton btnListarClipsStreamers;
    JTextField txtCantidadSuscripcion;
    DateTimePicker dpFechaLimiteSuscripcion;
    JButton btnAltaSuscripcion;
    JButton btnModificarSuscripcion;
    JButton btnEliminarSuscripcion;
    JList listUsuariosSuscripciones;
    JTextField txtCategoriaJuego;
    JTextField txtTituloJuego;
    JButton btnEliminarJuego;
    JButton btnModificarJuego;
    JButton btnAltaJuego;
    JList listStreamersJuegos;
    JButton btnListarStreamersJuegos;
    JTextField txtTituloClip;
    JTextField txtDuracionClip;
    JRadioButton rbPrimeUsuarioSi;
    JRadioButton rbPrimeUsuarioNo;
    JRadioButton rbPrivadoClipSi;
    JList listStreamersClips;
    JButton btnListarStreamersClips;
    JButton btnAltaClip;
    JButton btnModificarClip;
    JButton btnEliminarClip;
    JCheckBox cbTemaConfiguracion;
    JTextField txtFotoConfiguracion;
    JCheckBox cbOcultoConfiguracion;
    JButton btnAltaConfiguracion;
    JButton btnModificarConfiguracion;
    JButton btnEliminarConfiguracion;
    JList listUsuariosConfiguraciones;
    JButton btnListarUsuariosConfiguracion;
    JButton btnListarUsuariosSuscripciones;
    JRadioButton rbiPrivadoClipNo;
    JList listUsuarios;
    JList listStreamers;
    JList listSuscripciones;
    JList listJuegos;
    JList listClips;
    JList listConfiguraciones;
    JComboBox<Streamer> cbStreamersEnUsuarios;
    JButton btnAddStreamerAUsuario;


    JMenuItem conexionItem;
    JMenuItem salirItem;

    DefaultListModel<Usuario> dtmUsuarios;
    DefaultListModel<Streamer> dtmStreamersUsuarios;
    DefaultListModel<Suscripcion> dtmSuscripcionesUsuarios;
    DefaultComboBoxModel<Streamer> dcbmStreamers;


    DefaultListModel<Streamer> dtmStreamers;
    DefaultListModel<Usuario> dtmUsuariosStreamers;
    DefaultListModel<Clip> dtmClipsStreamers;

    DefaultListModel<Suscripcion> dtmSuscripciones;
    DefaultListModel<Usuario> dtmUsuariosSuscripciones;

    DefaultListModel<Juego> dtmJuegos;
    DefaultListModel<Streamer> dtmStreamersJuegos;

    DefaultListModel<Clip> dtmClips;
    DefaultListModel<Streamer> dtmStreamersClips;

    DefaultListModel<Configuracion> dtmConfiguraciones;
    DefaultListModel<Usuario> dtmUsuariosConfiguraciones;


    public Vista(){
        this.setTitle("Vista Principal");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setLocationRelativeTo(null);
        crearMenu();
        crearModelos();
    }

    private void crearModelos(){
        dtmUsuarios = new DefaultListModel<>();
        listUsuarios.setModel(dtmUsuarios);
        dtmStreamersUsuarios = new DefaultListModel<>();
        listStreamersUsuarios.setModel(dtmStreamersUsuarios);
        dtmSuscripcionesUsuarios = new DefaultListModel<>();
        listSuscripcionesUsuarios.setModel(dtmSuscripcionesUsuarios);
        dcbmStreamers = new DefaultComboBoxModel<>();
        cbStreamersEnUsuarios.setModel(dcbmStreamers);

        dtmStreamers = new DefaultListModel<>();
        listStreamers.setModel(dtmStreamers);
        dtmUsuariosStreamers = new DefaultListModel<>();
        listUsuariosStreamers.setModel(dtmUsuariosStreamers);
        dtmClipsStreamers = new DefaultListModel<>();
        listClipsStreamers.setModel(dtmClipsStreamers);

        dtmSuscripciones = new DefaultListModel<>();
        listSuscripciones.setModel(dtmSuscripciones);
        dtmUsuariosSuscripciones = new DefaultListModel<>();
        listUsuariosSuscripciones.setModel(dtmUsuariosSuscripciones);

        dtmJuegos = new DefaultListModel<>();
        listJuegos.setModel(dtmJuegos);
        dtmStreamersJuegos = new DefaultListModel<>();
        listStreamersJuegos.setModel(dtmStreamersJuegos);

        dtmClips = new DefaultListModel<>();
        listClips.setModel(dtmClips);
        dtmStreamersClips = new DefaultListModel<>();
        listStreamersClips.setModel(dtmStreamersClips);

        dtmConfiguraciones = new DefaultListModel<>();
        listConfiguraciones.setModel(dtmConfiguraciones);
        dtmUsuariosConfiguraciones = new DefaultListModel<>();
        listUsuariosConfiguraciones.setModel(dtmUsuariosConfiguraciones);
    }

    private void crearMenu(){
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");

        conexionItem = new JMenuItem("Conectar");
        conexionItem.setActionCommand("Conectar");

        salirItem = new JMenuItem("Salir");
        salirItem.setActionCommand("Salir");

        menu.add(conexionItem);
        menu.add(salirItem);
        barra.add(menu);
        this.setJMenuBar(barra);
    }

}
