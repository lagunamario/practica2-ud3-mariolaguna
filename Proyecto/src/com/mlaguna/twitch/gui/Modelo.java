package com.mlaguna.twitch.gui;

import com.mlaguna.twitch.*;
import com.mlaguna.twitch.util.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class Modelo {


    public void desconectar(){
        HibernateUtil.closeSessionFactory();
    }

    public void conectar(){
        HibernateUtil.buildSessionFactory();
    }


    public List<Usuario> getUsuarios(){
        return (List<Usuario>) HibernateUtil.getCurrentSession().createQuery("FROM Usuario").getResultList();
    }

    public List<Streamer> getStreamers(){
        return (List<Streamer>) HibernateUtil.getCurrentSession().createQuery("FROM Streamer").getResultList();
    }

    public List<Suscripcion> getSuscripciones(){
        return (List<Suscripcion>) HibernateUtil.getCurrentSession().createQuery("FROM Suscripcion").getResultList();
    }

    public List<Juego> getJuegos(){
        return (List<Juego>) HibernateUtil.getCurrentSession().createQuery("FROM Juego").getResultList();
    }

    public List<Clip> getClips(){
        return (List<Clip>) HibernateUtil.getCurrentSession().createQuery("FROM Clip").getResultList();
    }

    public List<Configuracion> getConfiguraciones(){
        return (List<Configuracion>) HibernateUtil.getCurrentSession().createQuery("FROM Configuracion").getResultList();
    }

    public void guardarUsuario(Usuario usuario){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(usuario);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void guardarStreamer(Streamer streamer){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(streamer);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void guardarSuscripcion(Suscripcion suscripcion){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(suscripcion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void guardarJuegos(Juego juego){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(juego);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void guardarClip(Clip clip){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(clip);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void guardarConfiguracion(Configuracion configuracion){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(configuracion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public Usuario getUsuario(int idUsuario){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Usuario WHERE id = :id");
        query.setParameter("id", idUsuario);
        return (Usuario) query.getSingleResult();
    }

    public Streamer getStreamer(int idStreamer){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Streamer WHERE id = :id");
        query.setParameter("id", idStreamer);
        return (Streamer) query.getSingleResult();
    }

    public Suscripcion getSuscripcion(int idSuscripcion){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Suscripcion WHERE id = :id");
        query.setParameter("id", idSuscripcion);
        return (Suscripcion) query.getSingleResult();
    }

    public Juego getJuego(int idJuego){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Juego WHERE id = :id");
        query.setParameter("id", idJuego);
        return (Juego) query.getSingleResult();
    }

    public Clip getClip(int idClip){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Clip WHERE id = :id");
        query.setParameter("id", idClip);
        return (Clip) query.getSingleResult();
    }

    public Configuracion getConfiguracion(int idConfiguracion){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Configuracion WHERE id = :id");
        query.setParameter("id", idConfiguracion);
        return (Configuracion) query.getSingleResult();
    }
    public void modificarUsuario(Usuario usuarioSeleccion){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(usuarioSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarStreamer(Streamer streamerSeleccion){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(streamerSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarSuscripcion(Suscripcion suscripcionSeleccion){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(suscripcionSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarJuego(Juego juegoSeleccion){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(juegoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarClip(Clip clipSeleccion){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(clipSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarConfiguracion(Configuracion configuracionSeleccion){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(configuracionSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrarUsuario(Usuario usuario){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(usuario);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrarStreamer(Streamer streamerBorrado){
        /*for (Usuario usuario : streamerBorrado.getUsuarios()) {
            usuario.setStreamers(null);
        }*/

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(streamerBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void registrarUsuariosEnStreamer(Streamer streamerSeleccionado){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        for (Usuario usuario : streamerSeleccionado.getUsuarios()) {
            usuario.setStreamers((List<Streamer>) streamerSeleccionado);
        }
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrarSuscripcion(Suscripcion suscripcion){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(suscripcion);
        sesion.getTransaction().commit();
    }

    public void borrarJuego(Juego juego){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(juego);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrarClip(Clip clip){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(clip);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrarConfiguracion(Configuracion configuracion){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(configuracion);
        sesion.getTransaction().commit();
        sesion.close();
    }





    /*public void altaUsuario(Usuario nuevoUsuario){
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoUsuario);
        sesion.getTransaction().commit();

        sesion.close();
    }

    public ArrayList<Usuario> getUsuarios(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Usuario");
        ArrayList<Usuario> lista = (ArrayList<Usuario>) query.getResultList();
        sesion.close();
        return lista;
    }

    public void modificar(Usuario usuarioSeleccion){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(usuarioSeleccion);
        sesion.close();
    }

    public void borrar(Usuario usuarioBorrado){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(usuarioBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void altaStreamer(Streamer nuevoStreamer){
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoStreamer);
        sesion.getTransaction().commit();

        sesion.close();
    }

    public ArrayList<Streamer> getStreamers(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Streamer");
        ArrayList<Streamer> lista = (ArrayList<Streamer>) query.getResultList();
        sesion.close();
        return lista;
    }

    public void modificar(Streamer streamerSeleccion){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(streamerSeleccion);
        sesion.close();
    }

    public void borrar(Streamer streamerBorrado){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(streamerBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void altaSuscripcion(Suscripcion nuevaSuscripcion){
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevaSuscripcion);
        sesion.getTransaction().commit();

        sesion.close();
    }

    public ArrayList<Suscripcion> getSuscripciones(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Suscripcion");
        ArrayList<Suscripcion> lista = (ArrayList<Suscripcion>) query.getResultList();
        sesion.close();
        return lista;
    }

    public void modificar(Suscripcion suscripcionSeleccion){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(suscripcionSeleccion);
        sesion.close();
    }

    public void borrar(Suscripcion suscripcionBorrado){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(suscripcionBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void altaJuego(Juego nuevoJuego){
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoJuego);
        sesion.getTransaction().commit();

        sesion.close();
    }

    public ArrayList<Juego> getJuegos(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Juego ");
        ArrayList<Juego> lista = (ArrayList<Juego>) query.getResultList();
        sesion.close();
        return lista;
    }

    public void modificar(Juego juegoSeleccion){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(juegoSeleccion);
        sesion.close();
    }

    public void borrar(Juego juegoBorrado){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(juegoBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void altaClip(Clip nuevoClip){
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoClip);
        sesion.getTransaction().commit();

        sesion.close();
    }

    public ArrayList<Clip> getClips(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Clip");
        ArrayList<Clip> lista = (ArrayList<Clip>) query.getResultList();
        sesion.close();
        return lista;
    }

    public void modificar(Clip clipSeleccion){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(clipSeleccion);
        sesion.close();
    }

    public void borrar(Clip clipBorrado){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(clipBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void altaConfiguracion(Configuracion nuevaConfiguracion){
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevaConfiguracion);
        sesion.getTransaction().commit();

        sesion.close();
    }

    public ArrayList<Configuracion> getConfiguraciones(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Configuracion");
        ArrayList<Configuracion> lista = (ArrayList<Configuracion>) query.getResultList();
        sesion.close();
        return lista;
    }

    public void modificar(Configuracion configuracionSeleccion){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(configuracionSeleccion);
        sesion.close();
    }

    public void borrar(Configuracion configuracionBorrado){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(configuracionBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    } */
}
